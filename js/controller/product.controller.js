const mainProducts = document.querySelector(".app-details .main-products");

// START QUANTITY CONTROL FUNCTION
export let qtyControl = {
  addProductToCart: () => {},
  upQty: () => {},
  downQty: () => {},
  upQtyCart: () => {},
  downQtyCart: () => {},
  deleteItem: () => {},
};
// END QUANTITY CONTROL FUNCTION

// START RENDER PRODUCT TO SCREEN
export let renderProducts = (productList, arrCart) => {
  mainProducts.innerHTML = "";
  productList.forEach((product) => {
    let productDIV = document.createElement("div");
    productDIV.classList.add(
      "product",
      "col-xl-3",
      "col-lg-4",
      "col-sm-6",
      "col-12",
      "p-2"
    );
    let productTop = `<div class="product__top text-success fw-semibold"><span>In Stock</span></div>`;
    let productImage = `<div class="product__img-container my-3 text-center"><img src="${product.img}" /></div>`;
    productDIV.innerHTML =
      "<div class='product__content px-3 py-4'>" +
      productTop +
      productImage +
      renderProductDetails(
        product.id,
        product.name,
        product.price,
        product.screen,
        product.backCamera,
        product.frontCamera,
        product.desc,
        arrCart
      ) +
      "</div>";
    mainProducts.append(productDIV);
    productDIV
      .querySelector(".product__details .details__bottom .add-product")
      .addEventListener("click", qtyControl.addProductToCart);
    productDIV
      .querySelector(
        ".product__details .details__bottom .fa-circle-chevron-right"
      )
      .addEventListener("click", qtyControl.upQty);
    productDIV
      .querySelector(
        ".product__details .details__bottom .fa-circle-chevron-left"
      )
      .addEventListener("click", qtyControl.downQty);
  });
};

// start adding product details
let renderProductDetails = (
  id,
  name,
  price,
  screen,
  backCamera,
  frontCamera,
  desc,
  arrCart
) => {
  let indexOfItemInCart = arrCart.findIndex((item) => {
    return item.product.id == id;
  });
  let productDetailsTop = `<div class="details__top"><h4>${name}</h4></div>`;
  let productDetailsBody = `<div class="details__body my-3">
      <p class="mb-1 text-muted">${desc}</p>
      <p><span class="fw-semibold">Màn hình:</span> ${screen}</p>
      <p><span class="fw-semibold">Camera sau:</span> ${backCamera}</p>
      <p><span class="fw-semibold">Camera trước:</span> ${frontCamera}</p>
      </div>`;
  let productDetailsBottom = `<div class="details__bottom d-flex justify-content-between align-items-center fs-5 mx-2">
      <p class="product__price price-highlight">${numberWithCommas(price)} đ</p>
      <div class="quantity-control" data-id="${id}">
      <span class="add-product btn btn-product" data-id="${id}"
      ${indexOfItemInCart != -1 && "style='display: none'"}
      >Thêm</span>
      <span class="quantity-adjust"
      ${indexOfItemInCart == -1 && "style='display: none'"}
      ><span class="fa-solid fa-circle-chevron-left gradient-text" data-id="${id}"></span>
      <span class="quantity">${
        indexOfItemInCart != -1 && arrCart[indexOfItemInCart].quantity
      }</span>
      <span class="fa-solid fa-circle-chevron-right gradient-text" data-id="${id}"></span>
      </span>
      </div>
      </div>`;
  return `<div class="product__details">${productDetailsTop}${productDetailsBody}${productDetailsBottom}</div>`;
};
// end adding product details
// END RENDER PRODUCT TO SCREEN

// START CALCULATE TOTAL PAYMENT
let calcTotalPayment = (arrCart) => {
  return arrCart.reduce((a, b) => {
    return a + b.totalPrice();
  }, 0);
};
// END CALCULATE TOTAL PAYMENT

// START RENDER CART TO SCREEN
export let renderCart = (arrCart) => {
  let cartDetails = document.getElementById("cart-details");
  if (arrCart.length == 0) {
    cartDetails.innerHTML = `<p class="fs-5">Thật là trống trải, hãy cùng nhau lấp đầy giỏ hàng này nào</p>`;
    document.getElementById("total-payment").innerText = "";
  } else {
    cartDetails.innerHTML = "";
    arrCart.forEach((item) => {
      let { id, name, img } = item.product;
      let cartItem = document.createElement("div");
      cartItem.classList.add("cart__item", "row", "my-3");
      cartItem.dataset.id = id;
      cartItem.innerHTML = `<div class="col-2"><img src="${img}" /></div>
        <span class="col-4 fw-semibold">${name}</span>
        <span class="quantity-adjust col-2 text-center">
        <span class="fa-solid fa-circle-chevron-left gradient-text" data-id="${id}"></span>
        <span class="quantity">${item.quantity}</span>
        <span class="fa-solid fa-circle-chevron-right gradient-text" data-id="${id}"></span>
        </span>
        <span class="item__totalPrice price-highlight col-3 text-end">${numberWithCommas(
          item.totalPrice()
        )} đ</span>
        <span class="fa-solid fa-trash item__delete col-1 text-secondary" data-id="${id}"></span>`;
      cartDetails.append(cartItem);
      cartItem
        .querySelector(".quantity-adjust .fa-circle-chevron-left")
        .addEventListener("click", qtyControl.downQtyCart);
      cartItem
        .querySelector(".quantity-adjust .fa-circle-chevron-right")
        .addEventListener("click", qtyControl.upQtyCart);
      cartItem
        .querySelector(".item__delete")
        .addEventListener("click", qtyControl.deleteItem);
    });
    document.getElementById(
      "total-payment"
    ).innerHTML = `Tổng cộng: <span class="price-highlight">${numberWithCommas(
      calcTotalPayment(arrCart)
    )} đ</span>`;
  }
};
// END RENDER CART TO SCREEN

// START RENDER NUMBER OF TOTAL ITEM IN CART
export let renderNumberOfItemsInCart = (arrCart) => {
  if (arrCart.length != 0) {
    document.querySelector(
      ".app__header .header__cart .cart__number"
    ).style.display = "flex";
    document.getElementById("cart-item-number").innerText = arrCart.length;
  } else {
    document.querySelector(
      ".app__header .header__cart .cart__number"
    ).style.display = "none";
  }
};
// END RENDER NUMBER OF TOTAL ITEM IN CART

// START RENDER UPDATED QUANTITY TO PRODUCT AND CART FIELDS
export let renderUpdatedQty = (
  productID,
  arrCart,
  indexOfItemInCart,
  isUpdateProductQty,
  isUpdateCartQty
) => {
  if (isUpdateCartQty) {
    document.querySelector(
      `#cart-details .cart__item[data-id='${productID}'] .quantity-adjust .quantity`
    ).innerText = arrCart[indexOfItemInCart].quantity;
    document.querySelector(
      `#cart-details .cart__item[data-id='${productID}'] .item__totalPrice`
    ).innerText = `${numberWithCommas(
      arrCart[indexOfItemInCart].totalPrice()
    )} đ`;
    document.getElementById(
      "total-payment"
    ).innerHTML = `Tổng cộng: <span class="price-highlight">${numberWithCommas(
      calcTotalPayment(arrCart)
    )} đ</span>`;
  }
  if (isUpdateProductQty) {
    document.querySelector(
      `.product__details .details__bottom .quantity-control[data-id='${productID}'] .quantity`
    ).innerText = arrCart[indexOfItemInCart].quantity;
  }
};
// END RENDER UPDATED QUANTITY TO PRODUCT AND CART FIELDS

// START ADJUST THE VISIBILITY OF PRODUCT'S ADD BUTTON AND QUANTITY CONTROL BAR
export let adjustQtyControlVisible = (
  productID,
  displayOfAddButton,
  displayOfQtyAdjustBar
) => {
  document.querySelector(
    `.product__details .details__bottom .quantity-control[data-id='${productID}'] .add-product`
  ).style.display = displayOfAddButton;
  document.querySelector(
    `.product__details .details__bottom .quantity-control[data-id='${productID}'] .quantity-adjust`
  ).style.display = displayOfQtyAdjustBar;
};
// END ADJUST THE VISIBILITY OF PRODUCT'S ADD BUTTON AND QUANTITY CONTROL BAR

// START RENDER ORDER DETAILS
export let renderOrderDetails = (arrCart) => {
  let orderContent = "";
  arrCart.forEach((item, i) => {
    let itemContent = `<tr class="order__item text-center">
    <td scope="row">${i + 1}</td>
    <td class="text-start">${item.product.name}</td>
    <td>${numberWithCommas(item.product.price)} đ</td>
    <td>${item.quantity}</td>
    <td class="text-end">${numberWithCommas(item.totalPrice())} đ</td>
    </tr>`;
    orderContent += itemContent;
  });
  orderContent += `<tr class="total-payment"><td class="text-center" scope="row" colspan="3">Tổng cộng:</td><td class="text-end price-highlight" colspan="2">${numberWithCommas(
    calcTotalPayment(arrCart)
  )} đ</td></tr>`;
  document.querySelector("#order-details table tbody").innerHTML = orderContent;
};
// END RENDER ORDER DETAILS

// START UTILITIES
let numberWithCommas = (num) => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
// END UTILITIES
