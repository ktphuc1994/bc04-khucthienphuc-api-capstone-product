import {
  renderProducts,
  qtyControl,
  renderCart,
  renderNumberOfItemsInCart,
  renderUpdatedQty,
  adjustQtyControlVisible,
  renderOrderDetails,
} from "./controller/product.controller.js";
import { cartItem } from "./model/product.model.js";

const BASE_URL = "https://62db6ca0d1d97b9e0c4f3326.mockapi.io/";
const PRODUCT_ENDPOINT = "mainProducts/";
const LOCALSTORAGE_CART = "CartITEMS";
const orderModal = new bootstrap.Modal("#order");
const cartModal = new bootstrap.Modal("#cart");

let arrCart = [];
let tempArrCart = localStorage.getItem(LOCALSTORAGE_CART);
if (tempArrCart != null) {
  arrCart = JSON.parse(tempArrCart);
  arrCart = arrCart.map((item) => {
    return new cartItem(item.product, item.quantity);
  });
}

// START RENDER PRODUCTS
axios({
  url: BASE_URL + PRODUCT_ENDPOINT,
  method: "GET",
})
  .then((res) => {
    // console.log(res);
    renderProducts(res.data, arrCart);
    renderNumberOfItemsInCart(arrCart);
  })
  .catch((err) => {
    console.log(err);
  });
// END RENDER PRODUCTS

// START FILTER PRODUCTS
let renderProductsByBrand = () => {
  // console.log("yes");
  // console.log(e.target.value);
  axios({
    url: BASE_URL + PRODUCT_ENDPOINT,
    method: "GET",
  })
    .then((res) => {
      let phoneType = document.getElementById("phone-brand-select").value;
      let filterProducts = [];
      switch (phoneType) {
        case "all":
          renderProducts(res.data, arrCart);
          break;
        default:
          filterProducts = res.data.filter((product) => {
            return product.type == phoneType;
          });
          renderProducts(filterProducts, arrCart);
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
document
  .getElementById("phone-brand-select")
  .addEventListener("change", renderProductsByBrand);
// END FILTER PRODUCTS

// START ADD PRODUCT TO CART
qtyControl.addProductToCart = (e) => {
  // console.log("yes");
  // console.log(e.target.getAttribute("data-id"));
  let id = e.target.getAttribute("data-id");
  axios({
    url: BASE_URL + PRODUCT_ENDPOINT + id,
    method: "GET",
  })
    .then((res) => {
      arrCart.push(new cartItem(res.data, 1));
      localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
      renderNumberOfItemsInCart(arrCart);
      adjustQtyControlVisible(id, "none", "inline");
      document.querySelector(
        `.product__details .details__bottom .quantity-control[data-id='${id}'] .quantity`
      ).innerText = 1;
      console.log(arrCart);
    })
    .catch((err) => {
      console.log(err);
    });
};
// END ADD PRODUCT TO CART

// START PRODUCT QUANTITY UP
qtyControl.upQty = (e) => {
  // console.log("yes");
  let id = e.target.getAttribute("data-id");
  let index = arrCart.findIndex((item) => {
    return item.product.id == id;
  });
  arrCart[index].quantity++;
  localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
  renderUpdatedQty(id, arrCart, index, true, false);
  // console.log("arrCart[index].quantity: ", arrCart[index].quantity);
};
// END PRODUCT QUANTITY UP

// START PRODUCT QUANTITY DOWN
qtyControl.downQty = (e) => {
  let id = e.target.getAttribute("data-id");
  let index = arrCart.findIndex((item) => {
    return item.product.id == id;
  });
  if (arrCart[index].quantity != 1) {
    arrCart[index].quantity--;
    localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
    renderUpdatedQty(id, arrCart, index, true, false);
    // console.log("arrCart[index].quantity: ", arrCart[index].quantity);
  } else {
    arrCart.splice(index, 1);
    localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
    renderNumberOfItemsInCart(arrCart);
    adjustQtyControlVisible(id, "inline", "none");
  }
};
// END PRODUCT QUANTITY DOWN

// START CART BUTTON
document.getElementById("btnCart").addEventListener("click", () => {
  renderCart(arrCart);
});
// END CART BUTTON

// START QUANTITY UP IN CART
qtyControl.upQtyCart = (e) => {
  let id = e.target.getAttribute("data-id");
  let index = arrCart.findIndex((item) => {
    return item.product.id == id;
  });
  arrCart[index].quantity++;
  localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
  renderUpdatedQty(id, arrCart, index, true, true);
  // console.log("arrCart[index].quantity: ", arrCart[index].quantity);
};
// END QUANTITY UP IN CART

// START QUANTITY DOWN IN CART
qtyControl.downQtyCart = (e) => {
  let id = e.target.getAttribute("data-id");
  let index = arrCart.findIndex((item) => {
    return item.product.id == id;
  });
  if (arrCart[index].quantity != 1) {
    arrCart[index].quantity--;
    localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
    renderUpdatedQty(id, arrCart, index, true, true);
    // console.log("arrCart[index].quantity: ", arrCart[index].quantity);
  } else {
    arrCart.splice(index, 1);
    renderCart(arrCart);
    localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
    renderNumberOfItemsInCart(arrCart);
    adjustQtyControlVisible(id, "inline", "none");
  }
};
// END QUANTITY DOWN IN CART

// START DELETE ITEM IN CART
qtyControl.deleteItem = (e) => {
  let id = e.target.getAttribute("data-id");
  let index = arrCart.findIndex((item) => {
    return item.product.id == id;
  });
  arrCart.splice(index, 1);
  localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
  renderCart(arrCart);
  renderNumberOfItemsInCart(arrCart);
  adjustQtyControlVisible(id, "inline", "none");
};
// END DELETE ITEM IN CART

// START CLICK "THANH TOÁN" BUTTON
document.getElementById("thanh-toan").addEventListener("click", () => {
  if (arrCart.length == 0) {
    document.getElementById("cart-details").innerHTML =
      "<p class='fs-5 text-danger'>Úi. Giỏ hàng đang trống kìa. Hãy thêm sản phẩm vào để thanh toán bạn nhé.</p>";
  } else {
    orderModal.show();
    cartModal.hide();
    renderOrderDetails(arrCart);
  }
});
// END CLICK "THANH TOÁN" BUTTON

// START CLICK "ĐẶT HÀNG" BUTTON
document.getElementById("btn-order").addEventListener("click", (e) => {
  arrCart = [];
  localStorage.setItem(LOCALSTORAGE_CART, JSON.stringify(arrCart));
  renderProductsByBrand();
  renderNumberOfItemsInCart(arrCart);
});
// END CLICK "ĐẶT HÀNG" BUTTON
